import { REQUEST_ACCESS, SEARCH_ENTERPRISES } from '../actions/actionTypes';
import { combineReducers } from 'redux';

const initialState = {
  token: '',
  uid: '',
  'client': '',
  'isLoading': false,
  'logged': false,
  'enterprises': ''
};

export const Reducer = (state = initialState, action) => {
  switch (action.type) {
    case REQUEST_ACCESS:
      return {
        ...state,
        token: action.token,
        uid: action.uid,
        client: action.client,
        isLoading: action.isLoading,
        logged: action.logged
      };
    case SEARCH_ENTERPRISES:
      return {
        ...state,
        enterprises: action.enterprises
      };
    default:
      return state;
  }
};

export const Reducers = combineReducers({
  requestAccess: Reducer,
  searchEnterprises: Reducer
});