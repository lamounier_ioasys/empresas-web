import { REQUEST_ACCESS, SEARCH_ENTERPRISES } from './actionTypes';

export const requestAccess = (credentials) => {

	let data = {
	  method: 'POST',
	  body: JSON.stringify(credentials),
	  headers: {
	  	'Cache-Control': 'no-cache',
	    'Content-Type': 'application/json',
	  }
	}

	const info = {}

	return function(dispatch) {
		dispatch({
		  type: REQUEST_ACCESS,
		  isLoading: true
		}) 

		return fetch("https://empresas.ioasys.com.br/api/v1/users/auth/sign_in", data).then(response => {

	  	if (response.statusText !== 'OK') return;

	  	let token = response.headers.get('access-token');
	  	let uid = response.headers.get('uid');
	  	let client = response.headers.get('client');

	  	info.token = token;
	  	info.uid = uid;
	  	info.client = client;

	  	dispatch({
			  type: REQUEST_ACCESS,
			  token: info.token,
			  uid: info.uid,
			  client: info.client,
			  isLoading: false,
			  logged: true
			}) 
	  }).catch(err => {
	    console.log(err);
	  })
  };
};

export const searchEnterprises = (info) => {

	let data = {
	  method: 'GET',
	  headers: {
	  	'Cache-Control': 'no-cache',
	    'Content-Type': 'application/json',
	    'access-token': info.token,
	    'client': info.client,
	    'uid': info.uid
	  }
	}

	return function(dispatch) {
		return fetch("https://empresas.ioasys.com.br/api/v1/enterprises?name=" + info.value, data).then(response => {
	  	response.json().then(value => {
	  		dispatch({
				  type: SEARCH_ENTERPRISES,
				  enterprises: value.enterprises
				}) 
	  	})
	  }).catch(err => {
	    throw err;
	  });
  };
};