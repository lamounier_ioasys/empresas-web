import React from 'react';
import { Redirect } from 'react-router-dom';
import { Formik } from 'formik';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Store } from '../../../store/';
import { requestAccess } from '../../../store/actions';

import icon_email from '../../../assets/images/icon-email.svg';
import icon_lock from '../../../assets/images/icon-lock.svg';

const FormLogin = (props) => {
	const { requestAccess } = props;
	const redirect = <Redirect to='/dashboard/' />;

	return (
		<React.Fragment>
			{Store.getState().requestAccess.logged ? redirect : ''}

			<Formik
	      initialValues={{ email: 'testeapple@ioasys.com.br', password: '12341234' }}

	      validate={values => {
	        const errors = {};

	        if (!values.email) {
	          errors.email = 'Required';
	        } else if (!values.password) {
	        	errors.password = 'Required';
	        } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)) {
	          errors.email = 'Invalid email address';
	        } return errors;
	      }}

	      onSubmit={(values, { setSubmitting }) => {
          setSubmitting(false);

          let credentials = { "email" : values.email, "password" : values.password }
          requestAccess(credentials);
	      }}
	    >
	      {({
	        values,
	        errors,
	        touched,
	        handleChange,
	        handleBlur,
	        handleSubmit,
	        isSubmitting,
	      }) => (
	        <form className="wrapper-form" onSubmit={handleSubmit}>
	        	<div className={
	        		Store.getState().requestAccess.isLoading ? 'message active' : 'message'
	        	}>
	        		<span>Validando informações</span>
	        	</div>

	        	<div className={errors.length > 0 ? 'message active' : 'message'}>
	        		<span>Usuario ou senha estão errados</span>
        		</div>

		        <div className="field-group">
			        <div className="icon">
								<img src={icon_email} alt="Ícone" />
							</div>

		          <input
		          	className="field"
		            type="email"
		            name="email"
		            placeholder="E-mail"
		            onChange={handleChange}
		            onBlur={handleBlur}
		            value={values.email}
		          />
		          
		          <span>{errors.email && touched.email && errors.email}</span>
		        </div>

		        <div className="field-group">
			        <div className="icon">
								<img src={icon_lock} alt="Ícone" />
							</div>

		          <input
		          	className="field"
		            type="password"
		            name="password"
		            placeholder="Senha"
		            onChange={handleChange}
		            onBlur={handleBlur}
		            value={values.password}
		          />

		          <span>{errors.password && touched.password && errors.password}</span>
		        </div>

	          <div className="form-submit" disabled={isSubmitting}>
							<button type="submit">Entrar</button>
						</div>
	        </form>
	      )}
	    </Formik>
		</React.Fragment>
	)
}

const mapStateToProps = store => ({
  token: store.requestAccess.token,
  uid: store.requestAccess.uid,
  client: store.requestAccess.client,
  isLoading: store.requestAccess.isLoading
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({ requestAccess }, dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(FormLogin);