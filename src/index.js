import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Store } from './store';
import { BrowserRouter, Switch, Route } from 'react-router-dom'

import './index.css';

import Login from './containers/Login';
import Dashboard from './containers/Dashboard';

ReactDOM.render(
	<Provider store={Store}>
		<BrowserRouter>
			<Switch>
				<Route path="/" exact={true} component={Login} />
				<Route path="/dashboard" component={Dashboard} />
			</Switch>
		</ BrowserRouter>
	</ Provider>, document.getElementById('root'));


