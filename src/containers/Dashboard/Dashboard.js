import React, { useState } from 'react';
import { Store } from '../../store/';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { searchEnterprises } from '../../store/actions';

import logo from '../../assets/images/logo-dashboard.png';
import icon_search from '../../assets/images/icon-search.svg';
import icon_close from '../../assets/images/icon-close.svg';

import './Dashboard.scss';

const Dashboard = (props) => {
	const [MainText, setMainText] = useState('Clique na busca para iniciar');
	const { searchEnterprises } = props;

	const token = Store.getState().requestAccess.token;
	const uid = Store.getState().requestAccess.uid;
	const client = Store.getState().requestAccess.client;

	const logged = Store.getState().requestAccess.logged;

	const setVisibleSearch = (e) => {
		
		let button = e.target;
		let headline = button.parentElement.parentElement.parentElement;
		let field = headline.querySelector('input');

		headline.classList.add('search');

		setTimeout(() => field.focus(), 100);
	}

	const setHiddenSearch = (e) => {

		let button = e.target;
		let headline = button.parentElement.parentElement.parentElement;

		headline.classList.remove('search');

		setMainText('Clique na busca para iniciar');
	}

	const fieldOnChange = (e) => {

		let field = e.target;
		let value = field.value;

		setMainText('Nenhum resultado encontrado');

		let auth = {
			token,
			client,
			uid,
			value
		}

		searchEnterprises(auth);
	}

	const EnterpriseItem = () => {
		const listItems = Store.getState().searchEnterprises.enterprises.map((enterprise) =>
			<div className="item" key={enterprise.id}>
				<div className="item-count">{enterprise.id}</div>

				<div className="item-content">
					<div className="item-content--title">{enterprise.enterprise_name}</div>
					<div className="item-content--subtitle">{enterprise.city}</div>
					<div className="item-content--country">{enterprise.country}</div>
				</div>
			</div>
		)

		return (
			<div className="items">{listItems}</div>
		)
	}

	return (
		<main className="page-dashboard">
			{logged ? '' : <Redirect to="/"/>}

			<div className="headline">
				<div className="logo">
					<img src={logo} alt="Logo - Ioasys" />
				</div>

				<div className="search">
					<button type="button" title="Pesquisar" aria-label="Pesquisar" onClick={setVisibleSearch}>
						<img src={icon_search} alt="Icone" />
					</button>
				</div>

				<form className="search-form">
					<button type="submit" className="icon">
						<img src={icon_search} alt="Icone" />
					</button>
					
					<input
						type="text"
						placeholder="Pesquisar..."
						onChange={fieldOnChange}
					 />

					 <button type="button" className="close" onClick={setHiddenSearch}>
					 	<img src={icon_close} alt="Icone" />
					 </button>
				</form>
			</div>

			<div className="wrapper">
			{Store.getState().searchEnterprises.enterprises ? <EnterpriseItem /> :
				<span><span>{MainText}</span></span>
			}
			</div>
		</main>
	)
}

const mapStateToProps = store => ({
	enterprises: store.searchEnterprises.enterprises
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({ searchEnterprises }, dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);