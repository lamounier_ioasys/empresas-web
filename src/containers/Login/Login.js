import React from 'react';

import logo from '../../assets/images/logo.png';

import FormLogin from '../../components/Forms/FormLogin';

import './Login.scss';

const App = () => {
	return (
		<main className="page-login">
			<div className="container">
				<div className="wrapper">
					<div className="wrapper-logo">
						<img src={logo} alt="Logo - Ioasys"/>
					</div>

					<div className="wrapper-title">Bem-vindo ao<br/>empresas</div>

					<div className="wrapper-description">
						<p>Digite abaixo o login e senha<br/>para acessar o painel</p>
					</div>

					<FormLogin/>
				</div>
			</div>
		</main>
	)
}

export default App;